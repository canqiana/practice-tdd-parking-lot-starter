Objective: 

1. Code Review : review each other's homework with my teammates , I learn some new knowledge from it, such as strategy pattern and factory pattern.
1. TDD + Object-Oriented programming practice.

Reflective:  Fruitful and Challenging.

Interpretive:  During this day of study, I practice a lot of Object-Oriented programming and TDD, and I met some difficulties in the process of practicing.

Decisional:  I will practice TDD + Object-Oriented programming more in the future. I will also actively seek the help of teachers and classmates when I have problem.

