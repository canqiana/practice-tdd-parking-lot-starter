package com.parkinglot;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ParkingLot {
    public  int capacity = 10;
    private final List<Car> carsList = new ArrayList<>(capacity);

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public List<Car> getCarsList() {
        return carsList;
    }

    public ParkingTicket park(Car car) {
        if(carsList.size() < capacity){
            carsList.add(car);
            return new ParkingTicket(car.getId());
        }
        throw new NoAvailablePositionException();
    }

    public Car fetch(ParkingTicket ticket) {
        List<Car> cars = carsList.stream().filter(car -> car.getId() == ticket.getId()).collect(Collectors.toList());
        if(cars.isEmpty() || ticket.getIsUsed()){
            throw new UnrecognizedParkingTicketException();
        } else {
            ticket.setIsUsed(true);
            carsList.remove(cars.get(0));
            return cars.remove(0);
        }
    }

    public boolean isContains(ParkingTicket parkingTicket){
        return carsList.stream().anyMatch(car -> car.getId() == parkingTicket.getId());
    }

    public int getCapacity() {
        return capacity;
    }
}
