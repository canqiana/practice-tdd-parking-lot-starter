package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SuperSmartParkingBoy extends ParkingBoy{
    public SuperSmartParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
    }

    @Override
    public ParkingTicket park(Car car){
        return parkingLots.stream()
                .max(Comparator.comparingInt(parkingLot ->
                        (parkingLot.getCapacity() - parkingLot.getCarsList().size())
                                / (parkingLot.getCapacity() != 0 ? parkingLot.getCapacity() : 9999)))
                .map(parkingLot -> parkingLot.park(car)).orElseThrow(NoAvailablePositionException::new);
    }

}
