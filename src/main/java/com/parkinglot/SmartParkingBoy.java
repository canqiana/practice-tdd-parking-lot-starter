package com.parkinglot;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SmartParkingBoy extends ParkingBoy{
    public SmartParkingBoy(ParkingLot... parkingLots) {
        super(parkingLots);
    }

    @Override
    public ParkingTicket park(Car car){
        return parkingLots.stream()
                .max(Comparator.comparingInt(parkingLot -> (parkingLot.getCapacity() - parkingLot.getCarsList().size())))
                .map(parkingLot -> parkingLot.park(car)).orElseThrow(NoAvailablePositionException::new);
    }

}
