package com.parkinglot;

import java.util.Objects;

public class ParkingTicket {
    private int id;

    private boolean isUsed = false;

    public ParkingTicket(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public boolean getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(boolean used) {
        isUsed = used;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParkingTicket that = (ParkingTicket) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
