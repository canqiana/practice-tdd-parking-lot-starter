package com.parkinglot;

import java.util.Arrays;
import java.util.List;

public class ParkingBoy {
    protected final List<ParkingLot> parkingLots;
    public ParkingBoy(ParkingLot... parkingLots) {
        this.parkingLots = Arrays.asList(parkingLots);
    }

    public ParkingTicket park(Car car){
        return parkingLots.stream().filter(parkingLot -> parkingLot.getCarsList().size() < parkingLot.capacity)
                .findFirst().map(parkingLot -> parkingLot.park(car)).orElseThrow(NoAvailablePositionException::new);
    }

    public Car fetch(ParkingTicket ticket){
        for (ParkingLot parkingLot : parkingLots) {
            if(!ticket.getIsUsed() && parkingLot.isContains(ticket)){
                return parkingLot.fetch(ticket);
            }
        }
        throw new UnrecognizedParkingTicketException();
    }
}
