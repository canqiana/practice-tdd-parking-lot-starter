package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SuperSmartParkingBoyTest {
    @Test
    void should_return_first_parking_lot_ticket_when_park_given_2_available_parking_lot_and_super_smart_boy_and_car() {
    //given
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1,parkingLot2);
        parkingLot2.park(new Car(1));
    //when
        superSmartParkingBoy.park(new Car(2));
    //then
        assertEquals(1,parkingLot1.getCarsList().size());
        assertEquals(1,parkingLot2.getCarsList().size());
    }

    @Test
    void should_return_second_parking_lot_ticket_when_park_given_first_full_and_second_available_parking_lot_and_super_smart_boy_and_car() {
    //given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car(1);
    //when
        superSmartParkingBoy.park(car);
    //then
        assertEquals(0,parkingLot1.getCarsList().size());
        assertEquals(1,parkingLot2.getCarsList().size());
    }

    @Test
    void should_return_right_car_with_right_ticket_when_fetch_given_2_parking_lot_both_with_a_car_and_super_smart_boy_and_2_ticket() {
    //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car(1);
        Car car2 = new Car(2);
        ParkingTicket ticket1 = parkingLot1.park(car1);
        ParkingTicket ticket2 = parkingLot2.park(car2);
    //when
        Car fetchedCar1 = superSmartParkingBoy.fetch(ticket1);
        Car fetchedCar2 = superSmartParkingBoy.fetch(ticket2);
    //then
        assertEquals(car1.getId(),fetchedCar1.getId());
        assertEquals(car2.getId(),fetchedCar2.getId());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_2_parking_lot_and_super_smart_boy_and_wrong_ticket() {
    //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
    //when  then
        assertThrows(UnrecognizedParkingTicketException.class, () -> superSmartParkingBoy.fetch(new ParkingTicket(99)));
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_2_parking_lot_and_super_smart_boy_and_used_ticket() {
    //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket ticket = superSmartParkingBoy.park(new Car(1));
        superSmartParkingBoy.fetch(ticket);
    //when  then
        assertThrows(UnrecognizedParkingTicketException.class, () -> superSmartParkingBoy.fetch(ticket));
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_2_without_position_parking_lot_and_super_smart_boy_and_car() {
    //given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car(1);
    //when  then
        assertThrows(NoAvailablePositionException.class, () -> superSmartParkingBoy.park(car));
    }
}
