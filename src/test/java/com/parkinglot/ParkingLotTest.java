package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot_and_car() {
    //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car(1);
    //when
        ParkingTicket ticket = parkingLot.park(car);
     //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_and_ticket() {
    //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car(1);
    //when
        ParkingTicket ticket = parkingLot.park(car);
        Car fetchedCar = parkingLot.fetch(ticket);
    //then
        assertEquals(car,fetchedCar);
    }
    
    @Test
    void should_return_right_car_when_fetch_given_parking_lot_and_right_ticket() {
    //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car(1);
        Car car2 = new Car(2);
        ParkingTicket ticket1 = parkingLot.park(car1);
        ParkingTicket ticket2 = parkingLot.park(car2);
     //when
        Car fetchedCar1 = parkingLot.fetch(ticket1);
        Car fetchedCar2 = parkingLot.fetch(ticket2);
        //then
        assertEquals(car1,fetchedCar1);
        assertEquals(car2,fetchedCar2);
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        //when
        ParkingTicket ticket = parkingLot.park(new Car(1));
        //then
        assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(new ParkingTicket(99)));
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_parking_lot_and_used_ticket() {
    //given
        ParkingLot parkingLot = new ParkingLot(10);
     //when
        ParkingTicket ticket = parkingLot.park(new Car(1));
        parkingLot.fetch(ticket);
     //then
        assertThrows(UnrecognizedParkingTicketException.class, () -> parkingLot.fetch(ticket));
    }

    @Test
    void should_return_null_when_park_given_without_position_parking_lot_and_car() {
        //given
        ParkingLot parkingLot = new ParkingLot(1);
        for (int i = 0; i < parkingLot.getCapacity(); i++) {
            parkingLot.park(new Car(i));
        }
        //when  then
        assertThrows(NoAvailablePositionException.class,() -> parkingLot.park(new Car(99)));
    }

}
