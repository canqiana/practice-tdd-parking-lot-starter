package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingBoyTest {
    @Test
    void should_return_first_parking_lot_ticket_when_park_given_2_available_parking_lot_and_parking_boy_and_car() {
    //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1,parkingLot2);
        Car car = new Car(1);
    //when
        parkingBoy.park(car);
    //then
        assertEquals(1,parkingLot1.getCarsList().size());
        assertEquals(0,parkingLot2.getCarsList().size());
    }

    @Test
    void should_return_second_parking_lot_ticket_when_park_given_first_full_and_second_available_parking_lot_and_parking_boy_and_car() {
    //given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car(1);
    //when
        parkingBoy.park(car);
    //then
        assertEquals(0,parkingLot1.getCarsList().size());
        assertEquals(1,parkingLot2.getCarsList().size());
    }

    @Test
    void should_return_right_car_with_right_ticket_when_fetch_given_2_parking_lot_both_with_a_car_and_parking_boy_and_2_ticket() {
    //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car(1);
        Car car2 = new Car(2);
        ParkingTicket ticket1 = parkingLot1.park(car1);
        ParkingTicket ticket2 = parkingLot2.park(car2);
    //when
        Car fetchedCar1 = parkingBoy.fetch(ticket1);
        Car fetchedCar2 = parkingBoy.fetch(ticket2);
    //then
        assertEquals(car1.getId(),fetchedCar1.getId());
        assertEquals(car2.getId(),fetchedCar2.getId());
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_2_parking_lot_and_parking_boy_and_wrong_ticket() {
    //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
    //when  then
        assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.fetch(new ParkingTicket(99)));
    }

    @Test
    void should_throw_exception_with_error_message_when_fetch_given_2_parking_lot_and_parking_boy_and_used_ticket() {
    //given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car(1);
        ParkingTicket ticket = parkingBoy.park(car);
        parkingBoy.fetch(ticket);
    //when  then
        assertThrows(UnrecognizedParkingTicketException.class, () -> parkingBoy.fetch(ticket));
    }

    @Test
    void should_throw_exception_with_error_message_when_park_given_2_without_position_parking_lot_and_parking_boy_and_car() {
    //given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car(1);
    //when  then
        assertThrows(NoAvailablePositionException.class, () -> parkingBoy.park(car));
    }
}
